# NIE-MVI Eye detection using CNNs

## Introduction

For my Computational intelligence methods project I chose to evaluate CNN tracker performance on detecting eyes.

In particular I was interested in testing YOLO architecture.

I was inspired with a biometry course I took last year in which we tried to recognize ears using similar architectures.

Structure of the project:
- Technical part can be found in ```nie-mvi.ipynb```.
- Documentation is this file, ```README.md```.
- Presentation is in ```blogpost.pdf```.

## Architecture (YOLO)

![](yolo.png)

The original YOLO (You Only Look Once) was written by Joseph Redmon in a custom framework called Darknet. Darknet is a very flexible research framework written in low level languages and has produced a series of very good object detectors.

YOLO models after YOLOv3 are written by new authors and each have varying goals.

YOLO is one of the first object detection network to combine the problem of drawing bounding boxes and classifying in one end-to-end differentiable network (hence the name).

The main advantages of YOLO models are its speed and small size. Also the framework is open source which gives room for tweaking.

## Dataset

![](dataset.png)

I initially started to look for a large eye dataset. Unfortunately I was not satisfied as most eye dataset consisted of black and white close-up shots. I found detection there to be too trivial, and somewhat unappealing to present. 

So I shifted my focus to finding a face dataset and repurposing it to detect eyes. A labelled face dataset with marked landmarks (eye location) was my goal
As my dataset I considered a few choices:

- **LFW** (labelled faces in the wild) : an extensive dataset consisting of over 7000 images of faces with tagged landmark points. The main challenge was: the tagged landmarks were points, not bounding boxes which were needed for my architecture

- **CelebA** : yet another extensive dataset consisting of over 10 000 celebrity face images. The same issue persisted, I had eye locations as points and not as bounding boxes.

Other datasets such as **FlickrFaces** and **MAAD-Face** were also considered, alas they had mostly descriptive labels and no landmark locations, which didn't help me much.

In the end I opted to reuse the CelebA database, as it's image sizes were more standardized. I then augmented it into what I needed, a dataset of 1000 694x850 celebrity .jpg images with annotated bounding-box eye locations. I skipped pictures where the subject had sunglasses on, because my task was only to label and detect eyes.

## Annotation

![](labelling.png)

This was a simple but uninspiring task. LabelImg repository greatly aided me. LabelImg is a graphical image annotation tool.

It is written in Python and uses Qt for its graphical interface. This Python project allows quick and easy labelling in YOLO format.

The task itself is simple but remarkably tedious. Thankfully, with a lot of patience and the use of hotkeys the annotation was done in a reasonable amount of time.

In the end the final dataset consists of:
- 750 training images
- 125 testing images
- 125 validation images.

## Setup

The setup was simple. Essentially:
- downloading the YOLOv5 repository
- installing required Python requirements
- fetching weights
- configuring coco128.yaml
    - 1 class
    - paths for training, testing and validation datasets

I opted to use wandb to visualize and interpret my results, as I have used Tensorboard before and wanted to try wandb out.

## Initial runs

To begin with I ran 3 initial configurations:

<center>

| name   	| batches 	| epochs 	| size           	|
|--------	|---------	|--------	|----------------	|
| **YOLOv3** 	| 8       	| 25     	| yolov3-tiny.pt 	|
| **YOLOv5** 	| 8       	| 25     	| yolov5s.pt     	|
| **YOLOv7** 	| 8       	| 25     	| yolov7-tiny.pt 	|

</center>

And got the following results:

<center>

| name   	| precision 	| recall    	| mAP_0.5   	| mAP_0.5:0.95 	|
|--------	|-----------	|-----------	|-----------	|--------------	|
| YOLOv3 	| **0.984** 	| 0.976     	| 0.9846    	| 0.404        	|
| YOLOv5 	| 0.981     	| **0.976** 	| **0.989** 	| **0.443**    	|
| YOLOv7 	| 0.975     	| 0.932     	| 0.962     	| 0.407        	|

</center>

The initial results are very similar and strong right off the start. As we can see the differences in iterations are more accentuated in more specific tasks or longer train times. Because of that we chose to continue our experiments with YOLOv5 as it showed marginally better results and had the best mAP_0.5:0.95 value (average mAP over different IoU thresholds, from 0.5 to 0.95).

YOLOv5           |  YOLOv7 - falsepositives
:-------------------------:|:-------------------------:
![](v5initial.jpg)         |  ![](v7initial.jpg)

So our baseline for further work was to be:

<center>

| name   	| precision 	| recall    	| mAP_0.5   	| mAP_0.5:0.95 	|
|--------	|-----------	|-----------	|-----------	|--------------	|
| YOLOv5 	| 0.981     	| 0.976 	| 0.989 	| 0.443    	|

</center>

## Improvements

### Batch size

I immediately noticed that plenty of literature uses batch sizes significantly larger than 8. And as I was using cloud resources over Kaggle I decided to try more.

The author of YOLOv5, Glenn Jocher, did some testing himself but his results deviated to at most 0.001 for all metrics.

![](authorbatch.png)

By assigning batch size 30 the results improved about as much as Jocher initially reported.

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|
|----------------	|-----------	|--------	|---------	|--------------	|
| exp1           	| 0.991     	| 0.976  	| 0.987   	| 0.439        	|
| exp2           	| 0.993     	| 0.972  	| 0.991   	| 0.445        	|

</center>

Curiously enough the validation set results during training were more successful, mAP_0.5:0.95 jumped by as much as 2%. Futher experiments were done with increased batch size.

### Longer training

More epochs should naturally lead to better results? This one was sort of expected, the results seemed to already plateau in the first run, but just to be sure the number of epochs was increased from 25 to 40.

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|
|----------------	|-----------	|--------	|---------	|--------------	|
| exp1           	| 0.991     	| 0.976  	| 0.987   	| 0.439        	|
| exp3           	| 0.992     	| 0.976  	| 0.987   	| 0.432        	|

</center>

This produced nearly identical, possibly even marginally worse results and took more time to boot. If we had doubts of overfitting the original run there were no doubts here. For this particular example we shall stick to our initial baseline of 25 epochs.

### Early stopping

What about the opposite? Let's try and not overfit our data. YOLOv5 has a nice built-in parameter called ```--patience```. 

Patience essentially stops the training process if it doesn't see the model improve in a given number of epochs. Its default value is a staggering 100, towering over our meager 25 epochs in total, so as a first guess a value of 3 epochs was used.

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|
|----------------	|-----------	|--------	|---------	|--------------	|
| exp1           	| 0.991     	| 0.976  	| 0.987   	| 0.439        	|
| exp4           	| 0.954     	| 0.936  	| 0.939   	| 0.32         	|

</center>

The option triggered in the 10 epoch already. And while the validation set results were reasonably similar our performance on the hidden set took noticable hits (4% less P and R and 10% mAP).

Setting the patience to a higher value such as 5 produced nearly identical results.

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|
|----------------	|-----------	|--------	|---------	|--------------	|
| exp1           	| 0.991     	| 0.976  	| 0.987   	| 0.439        	|
| exp5           	| 0.993     	| 0.972  	| 0.991   	| 0.445        	|

</center>

This all pointed to the fact that the epoch number was most probably far too small for patience to make an impact - the potential time loss also did not make much of a difference.  

### Regularization

Further on we tackled regularization, trying to punish picking the same features over and over again. As such a change was needed to some default hyper-parameters.

```
weight_decay: 0.0015  # triple the default 0.0005
iou_t: 0.4  # double the default 0.2
```

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|      	
|----------------	|-----------	|--------	|---------	|--------------	|
|exp1           	| **0.991**     	| 0.976  	| 0.987   	| **0.439** |
| exp12          	| 0.988     	| **0.984**  	| **0.992**   	| 0.435 |

</center>

This turned out to yet again be a sideways movement. Marginal improvement was seen in R and mAP_0.5 but performance was lost in P and mAP_0.5:0.95. Other values of ```weight_decay``` and ```iou_t``` didn't deviate lots.

### Data augmentation

As one of our final efforts we turned to our input data and tried to augment it with Roboflow. A larger training dataset could lead to better end results.

The size of the training dataset jumped from 1000 to 1500.
To gain new training data the original images from the training dataset were: 

- cropped (0% to 20%)
- rotated (-15% to 15%)
- brightened (-25% to 25%)
- exposed (-25% to 25%)

![](augmented.png)

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|
|----------------	|-----------	|--------	|---------	|--------------	|
| exp1           	| 0.991     	| 0.976  	| 0.987   	| 0.439        	|
| exp16          	| 0.983     	| 0.968  	| 0.984   	| 0.451        	|

</center>

The results are very similar with a slight increase in the thresholded mAP. P and R stayed roughly the same. A minute improvement so to speak.

### Others

YOLOv5's ```evolve``` switch was also tested. It resembles a built in hyper-parameter evolution function, essentially a very computationally expensive  iterative hyper-parameter comparison. No specific hyper-parameter values were detected to be true performance outliers.

A larger scale YOLOv5 architecture was also put to the test, but yet again provided similar results.

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|
|----------------	|-----------	|--------	|---------	|--------------	|
| exp1           	| 0.991     	| 0.976  	| 0.987   	| 0.439        	|
| exp21   	| 0.995     	| 0.98   	| 0.984   	| 0.444        	|   	|

</center>

### Confidence threshold

At this point it had become apparent that the trained network had pretty much plateaued and only our interpretation could provide better results.

For our final experiments we tried using a different confidence threshold. Instead of the usual value of 50% we argued we want to be more certain before we classify.

<center>

| run            	| precision 	| recall 	| mAP_0.5 	| mAP_0.5:0.95 	|
|----------------	|-----------	|--------	|---------	|--------------	|
| exp12          	| 0.988     	| 0.984  	| 0.992   	| 0.435        	|
| exp12 conf 0.7 	| 1         	| 0.924  	| 0.962   	| 0.468        	|
| exp12 conf 0.8 	| 1         	| 0.012  	| 0.506   	| 0.203        	|

</center>

At long last some improvements! But the threshold should not be overdone. A value of 0.7 performed well while the R of 0.8 dropped to almost 0.

## Final thoughts

![](randomresult.png)

Improving on good initial results proved to be quite challenging.
Most of the tried techniques turned out to be questionable improvements - with slight improvements in one, coupled with weaker performance in other metrics.

We present a table that summarizes our findings:

<center>

|                                              	| improved results 	|                                         comment                                         	|
|:--------------------------------------------:	|:----------------:	|:---------------------------------------------------------------------------------------:	|
|                **batch size**                	|       **Y**      	|                                  **small improvements**                                 	|
|                **model size**                	|       **Y**      	|                                  **unsurprisingly better, but only small improvements**                                 	|
|                  more epochs                 	|         N        	|           severely overfitted our data potentially sensible in other scenarios          	|
|                early stopping                	|         N        	|          detrimented our performance needed much more with longer training time         	|
| regularization (weight decay, iou threshold) 	|         N        	|                                clever but no real effect                                	|
|             **data augmentation**            	|       **Y**      	|              **small improvements bigger dataset is almost always better**              	|
|           **confidence threshold**           	|       **Y**      	| **biggest improvement not significantly change in training but more in interpretation** 	|

</center>

The best run in this use-case would probably be:
- YOLOv5m
- on the augmented dataset
- 25 epochs
- early stopping at 5 just in case
- with higher confidence threshold 0.7

To conclude YOLOv5 is a very powerful tool, even right of the shelf but is quite hard to optimize and further improve.

